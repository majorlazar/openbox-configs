# Openbox Configs
The three files, viz, ***menu.xml*** ***autostart*** and ***rc.xml*** can be used as it is, given ***you've installed specified programs***, by placing them in the directory ***home/user/.config/openbox*** <br>

#### menu.xml-neo
The file, ***menu.xml-neo*** is to be used with dynamic menu generator program ***obmenu-generator***. It doesn't need to be placed by user, just running the ***obmenu-generator*** will create the file. But ***BEWARE*** the this will replace your ***existing menu.xml*** file. So, be sure to back that up somewhere else. 

#### old-keybindings.xml
This file contains the old window tiling keybindings which use if-else arguement to smartly detect window placement, with just four keybindings. Replace the relevant section in ***rc.xml*** with the content of this file. 
